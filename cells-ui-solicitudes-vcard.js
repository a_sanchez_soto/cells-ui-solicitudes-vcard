import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-solicitudes-vcard-styles.js';
import '@vcard-components/cells-util-behavior-vcard';
import '@vcard-components/cells-theme-vcard';
import '@cells-components/cells-select';
import '@bbva-web-components/bbva-button-action';
import '@cells-components/coronita-icons';
import '@cells-components/cells-icon';
import '@vcard-components/cells-ui-autocomplete-vcard';
import '@bbva-web-components/bbva-button-icon';
import '@bbva-web-components/bbva-form-number';
/**
This component ...

Example:

```html
<cells-ui-solicitudes-vcard></cells-ui-solicitudes-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/

const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiSolicitudesVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-solicitudes-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      isAdmin: {
        type: Boolean
      },
      territorios: {
        type: Array
      },
      oficinas: {
        type: Array
      },
      estados: {
        type: Array
      },
      items: {
        type: Array
      },
      territorioSelected: { 
        type: Object
      },
      oficinaSelected: { 
        type: Object
      },
      estadoSelected: { 
        type: Object
      },
      verSolicitud: {
        type: Boolean
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.isAdmin = (this.extract(this, 'userSession.perfil.codigo', '') === this.ctts.perfiles.admin);
    this.territorios = [];
    this.oficinas = [];
    this.estados = [];
    this.items = [];
    this.territorioSelected = {};
    this.oficinaSelected = {};
    this.estadoSelected = {};
    this.verSolicitud = true;
    let codigoPerfilSesion = this.extract(this, 'userSession.perfil.codigo', null);
    if(codigoPerfilSesion === this.ctts.perfiles.proveedor){
      this.verSolicitud = false;
    }

    this.addEventListener('clear-value-autocomplete',(event)=>{
      console.log('clear-value-autocomplete',event.detail);
      if(event.detail.name === 'autocomplete-territorio'){
        this.territorioSelected = {};
      }else if(event.detail.name === 'autocomplete-oficina') {
        this.oficinaSelected = {};
      }
    });

    this.addEventListener(this.events.loadDataCellsSelectComplete,async (event)=> {
      console.log(`Evento escuchado[${this.events.loadDataCellsSelectComplete}]`, event.detail)
      console.log(`codigoPerfilSesion[${codigoPerfilSesion}]`);
      let indice = 0;
      if(event.detail && event.detail.select && event.detail.select.id === 'estados-select'){
        if(codigoPerfilSesion === this.ctts.perfiles.oficina){
          event.detail.select.options.forEach((el,i) => {
            if(el.record.codigo === this.ctts.estados.solicitado){
              indice = i;
            }
          });  
        }else if(codigoPerfilSesion === this.ctts.perfiles.proveedor) {
          event.detail.select.options = event.detail.select.options.filter((opt)=>{
              return ( opt.record.codigo !== this.ctts.estados.anulado && 
                      opt.record.codigo !== this.ctts.estados.solicitado );
          });
          event.detail.select.options.forEach((el,i) => {
            if(el.record.codigo === this.ctts.estados.gestionado){
              indice = i;
            }
          });
        }
        if(codigoPerfilSesion !== this.ctts.perfiles.admin){
          event.detail.select.selected = indice;
        }
      }
      this.estadoSelected = this.getById('estados-select').options[indice];
      await this.requestUpdate();
      console.log('select estados', this.getById('estados-select').options[indice]);
      this.onSearch();
    })
    this.updateComplete.then(() => {
      this.inputSelectLoadData(this.events.loadDataCellsSelect, this);
      window.addEventListener('click', (e) => {
        this.hideActionsMenu();
      });
    });
  }
  
  hideActionsMenu(){
    let menus = this.shadowRoot.querySelectorAll('.list-actions');
    menus.forEach((item)=>{
        item.classList.add('no-show');  
    });
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-solicitudes-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  buildTarjetas(details) {
    if(details) {
      let arrDetail = [];
      details.forEach((detail)=>{
        arrDetail.push(detail.tarjeta.value);
      });

      return html`
        ${arrDetail.join(' - ')}
      `;
    }

    return html` `;

  }

  buildHeadAdmin() {
    if(this.isAdmin) {
      return html`<th>Oficina</th>`;
    }
    return html``;
  }

  buildColumnsAdmin(item) {
    if(this.isAdmin) {
      return html`<td>${this.extract(item, 'oficina.nombre')}</td>`;
    }
    return html``;
  }

  agregarPedido() {
    console.log(`CellsUiSolicitudesVcard.agregarPedido [EVENT] = ${this.events.agregarPedido}`);
    this.dispatch(this.events.agregarPedido,{});
  }

  anularSolicitud(item, index) {
    console.log(`CellsUiSolicitudesVcard.anularSolicitud.item(${index}) [EVENT] = ${this.events.anularSolicitud}`);
    this.dispatch(this.events.anularSolicitud, item);
  }

  detalleSolicitud(item, index) {
    console.log(`CellsUiSolicitudesVcard.detalleSolicitud.item(${index}) [EVENT] = ${this.events.detalleSolicitud}`);
    this.dispatch(this.events.detalleSolicitud, item);
  }

  editarSolicitud(item, index) {
    console.log(`CellsUiSolicitudesVcard.editarSolicitud.item(${index}) [EVENT] = ${this.events.editarSolicitud}`);
    this.dispatch(this.events.editarSolicitud, item);
  }

  promoverSolicitud(item, index) {
    console.log(`CellsUiSolicitudesVcard.promoverSolicitud.item(${index}) [EVENT] = ${this.events.editarSolicitud}`);
    this.dispatch(this.events.promoverSolicitud, item);
  }
  
  menuActions(event) {
    console.log(`CellsUiSolicitudesVcard.menuActions`);
    event.stopPropagation();
    let menus = this.shadowRoot.querySelectorAll('.list-actions');
    let node = event.path.filter((item)=> {
      return item.classList && item.classList.contains && item.classList.contains('content-actions');
    });
    
    let listActions = node[0].querySelector('.list-actions');
    menus.forEach((item)=>{
      if(item.id !== listActions.id){
        item.classList.add('no-show');  
      } 
    });
    
    if(!listActions.classList.contains('no-show')) {
      listActions.classList.add('no-show');
    } else {
      listActions.classList.remove('no-show');
    }
    
  }

  buildMenuActions(item, index) {
    let { codigo: codigoPerfil } = this.userSession.perfil;
    let { codigo: codigoEstado } = item.estado;
    let verEditar = true;
    let verAnular = true;
    let verPromover = true;
    let textPromover = 'Promover';
    let iconPromover = 'getout';
    if (codigoEstado === this.ctts.estados.anulado) {
      verAnular = false;
      verEditar = false;
      verPromover = false;
    } else if (codigoPerfil === this.ctts.perfiles.oficina) {

      if (codigoEstado !== this.ctts.estados.solicitado) {
        verAnular = false;
        verEditar = false;
        textPromover = 'Estado';
        iconPromover = 'transfer';
      }

      if (codigoEstado === this.ctts.estados.porentregar) {
        textPromover = 'Promover';
        iconPromover = 'getout';
      }

    } else if (codigoPerfil === this.ctts.perfiles.admin) {
      verEditar = true;
      verAnular = true;
      verPromover = true;
    } else if (codigoPerfil === this.ctts.perfiles.proveedor) {
      verEditar = false;
      verAnular = false;
      verPromover = true;
    }

    if (codigoEstado === this.ctts.estados.entregado) {
      verEditar = false;
      verAnular = false;
      verPromover = true;
      textPromover = 'Historial';
      iconPromover = 'historic';
    }

    return html`
      <div class = "list-actions no-show" id = "list-actions-${index}">
        <div ?hidden = "${!verEditar}"  @click = "${() => { this.editarSolicitud(item, index); }}" > 
        <cells-icon 
        class ="icon-actions" 
        size = "19" 
        icon="coronita:edit"
        ></cells-icon> Editar</div>

        <div ?hidden = "${!verAnular}" @click = "${() => { this.anularSolicitud(item, index); }}" > 
        <cells-icon 
        class ="icon-actions" 
        size = "20" 
        icon="coronita:close"
        ></cells-icon> Anular</div>

        <div @click = "${() => { this.detalleSolicitud(item, index); }}" class = "item-detalle"> 
        <cells-icon 
        class ="icon-actions" 
        size = "20" 
        icon="coronita:listview"
        ></cells-icon> Detalle</div>

        
        <div ?hidden = "${!verPromover}" @click = "${() => { this.promoverSolicitud(item, index); }}" class = "item-promover"> 
        <cells-icon 
        class ="icon-actions" 
        size = "20" 
        icon="coronita:${iconPromover}"
        ></cells-icon> ${textPromover}</div>
  
      </div>
    `;
  }

  onChangeTerritorio(event) {
    this.territorioSelected = event.detail;
    console.log('CellsUiSolicitudesVcard.onChangeTerritorio', event.detail);
    this.dispatch(this.events.changeTerritorio, event.detail);
  }

  onChangeOficina(event) {
    this.oficinaSelected = event.detail;
    console.log('CellsUiSolicitudesVcard.onChangeOficina', event.detail);
    this.dispatch(this.events.changeOficina, event.detail);
  }

  onChangeEstados(event) {
    this.estadoSelected = event.detail;
    console.log('CellsUiSolicitudesVcard.onChangeEstados', event.detail);
    this.dispatch(this.events.changeEstado, event.detail);
  }

  onSearch(event) {
    let params = {
      oficina: this.oficinaSelected,
      territorio: this.territorioSelected,
      estado: this.estadoSelected
    };

    if(this.getInputValue('idSolicitudInput')){
      params.codigo = this.getInputValue('idSolicitudInput');
    }

    let detail = {
      settings:{
        path: this.services.endPoints.solicitudes,
        onSuccess: async (response)=> {
          this.dispatch(this.events.globalSpinner, false);
          this.items = await response.detail;
        }
      },
      params: params 
    };
    console.log('CellsUiSolicitudesVcard.onSearchSolicitudes', detail);
    this.dispatch(this.events.searchSolicitudes, detail);
  }

  formatEstado(estado) {
    if(estado.codigo === 'SOLICITADO'){
      return 'color:#1464A5;';
    }else if(estado.codigo === 'GESTIONADO') {
      return 'color:#9C6C01;';
    }else if(estado.codigo === 'PRODUCCION') {
      return 'color:#5BBEFF;';
    }else if(estado.codigo === 'PORENTREGAR') {
      return 'color:#C65302;';
    }else if(estado.codigo === 'ENTREGADO') {
      return 'color:#028484;';
    }else if(estado.codigo === 'ANULADO') {
      return 'color:#B92A45;'; 
    }
    
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class = "panel-top" >
        <div class="section group">
            
            <div class="col span_${this.isAdmin ? '4' : '6'}_of_12">
            ${this.verSolicitud ? html`
                <button class = "button" @click = ${this.agregarPedido} >
                  <cells-icon 
                    size = "20" 
                    icon="coronita:add"
                    ></cells-icon> Agregar Pedido
                </button>
            ` : html`&nbsp;` }  
            
            </div>

            <div class="col span_${this.isAdmin ? '8' : '6'}_of_12 text-right">
              <div class="section group">
                <div   class="col span_4_of_12 content-select-filter" style = "text-align:left !important;">
                <bbva-form-number label= "ID Solicitud" id = "idSolicitudInput" ></bbva-form-number>  
                <cells-ui-autocomplete-vcard
                                ?hidden = ${true} 
                                name = "autocomplete-territorio"
                                bgClass = "white-bg" 
                                @selected-item="${this.onChangeTerritorio}"
                                label="Territorios"
                                .items = ${this.territorios}  
                                displayLabel="nombre"
                                displayValue="_id">
                  </cells-ui-autocomplete-vcard>
                </div>
                <div ?hidden = ${!this.isAdmin} class="col span_4_of_12 content-select-filter" style = "text-align:left !important;">
                  <cells-ui-autocomplete-vcard 
                                name = "autocomplete-oficina"
                                bgClass = "white-bg"
                                @selected-item="${this.onChangeOficina}"
                                label="Oficinas"
                                .items = ${this.oficinas}   
                                displayLabel="nombre"
                                displayValue="_id">
                  </cells-ui-autocomplete-vcard>
                </div>
                <div  class="col span_${this.isAdmin ? '4' : '8'}_of_12 content-select-filter">
                <div  class="col span_10_of_12 content-select-filter" style = "padding-right:0px !important;padding-left:0px !important;">
                
                <cells-select 
                  id = "estados-select"
                  data-label="nombre" 
                  data-value="_id" 
                  data-is-key-value= "true"
                  data-add-all = "true"
                  data-text-add-all = "Todos los estados"
                  data-path = "${this.services.endPoints.valores.estadosTc}"
                  label = "Estados"
                  @selected-option-changed="${(e) => { this.selectedOptionChanged(e, (event) => { this.onChangeEstados(event);});}}"
                  ></cells-select>

                </div>
                <div  class="col span_2_of_12 content-select-filter" >
                  <bbva-button-icon @click = "${this.onSearch}" icon="coronita:search"></bbva-button-icon>
                </div>  
                      
                </div>
              </div>
            </div>
        </div>    
      </div>

      <div class = "content-table-solicitudes content-table-vcard">
        <table class = "green" >
          <thead> 
            <tr>
              <th>ID Solicitud</th>
              <th>Tarjetas</th>
              <th>Estado</th>
              ${this.buildHeadAdmin()}
              <th>Fecha</th>
              <th style = "width : 25px;">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          ${this.items.map((item,index) => html`
            <tr>
                <td>${item.codigo}</td>
                <td>${this.buildTarjetas(item.details)} </td>
                <td style = "text-transform:uppercase;${this.formatEstado(item.estado)}" >${this.extract(item,'estado.nombre')}</td>
                ${this.buildColumnsAdmin(item)}
                <td>${this.formatDate(item.creacion)}</td>
                <td style = "width : 25px;" >
                  <div class = "content-actions">
                    <bbva-button-action 
                                class = "actionDetail" 
                                icon="coronita:configuration" 
                                @click = ${this.menuActions} ></bbva-button-action>

                    ${this.buildMenuActions(item,index)} 
                                
                  </div>
                </td>
              </tr>
          `)}
          </tbody>
        </table>
        ${this.items.length === 0 ? html` <div style = "width: 100%;
                                                        padding: 15px;
                                                        border: 1px solid #BDBDBD;
                                                        text-align: center;
                                                        color: #666;">Sin resultados que mostrar.</div>` : html``}
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiSolicitudesVcard.is, CellsUiSolicitudesVcard);
